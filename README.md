# iCollabrains

``` PowerShell
func new --name iHome --template "Http trigger" --authlevel "anonymous"
```


📦icollabrains  
 ┣ 📂src  
 ┃ ┣ 📂.vscode  
 ┃ ┃ ┗ 📜extensions.json  
 ┃ ┣ 📂iHome  
 ┃ ┃ ┣ 📜function.json  
 ┃ ┃ ┗ 📜run.ps1  
 ┃ ┣ 📂modules  
 ┃ ┃ ┗ 📂PSHTML  
  ┃ ┣ 📜.gitignore  
 ┃ ┣ 📜host.json  
 ┃ ┣ 📜local.settings.json  
 ┃ ┣ 📜profile.ps1  
 ┃ ┗ 📜requirements.psd1  
 ┣ 📂terraform  
 ┃ ┣ 📜backend.tf  
 ┃ ┣ 📜main.tf  
 ┃ ┣ 📜outputs.tf  
 ┃ ┣ 📜providers.tf  
 ┃ ┗ 📜variables.tf  
 ┣ 📜.gitlab-ci.yml  
 ┗ 📜README.md