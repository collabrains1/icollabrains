output "resource_group_name" {
  value = azurerm_resource_group.resource_group.name
}

output "resource_group_location" {
  value = azurerm_resource_group.resource_group.location
  description = "rg location"
}

output "storage_account_name" {
  value = azurerm_storage_account.storage_account.name
  description = "storage account name"
}