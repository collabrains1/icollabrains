resource "azurerm_resource_group" "resource_group" {
  name     = var.resource_group_name
  location = var.location
}


resource "azurerm_storage_account" "storage_account" {
  name                     = var.storage_account_name
  resource_group_name      = azurerm_resource_group.resource_group.name
  location                 = azurerm_resource_group.resource_group.location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_replication_type
}

resource "azurerm_app_service_plan" "app_service_plan" {
  name                = var.app_service_plan
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  sku {
    tier = var.app_service_plan_tier
    size = var.app_service_plan_size
  }
  
}

resource "random_id" "primary_slot" {
	  byte_length = 6
}
resource "azurerm_windows_function_app" "function_app" {
  name                       = var.function_app_name
  resource_group_name        = azurerm_resource_group.resource_group.name
  location                   = azurerm_resource_group.resource_group.location
  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  service_plan_id            = azurerm_app_service_plan.app_service_plan.id
  
  site_config {
    application_stack {
      powershell_core_version = "7.2"
      
    }
    use_32_bit_worker = false
    always_on         = true
  }
  app_settings = {
    WEBSITE_CONTENTSHARE = "${var.function_app_name}-${random_id.primary_slot.hex}"
    WEBSITE_CONTENTAZUREFILECONNECTIONSTRING = azurerm_storage_account.storage_account.primary_connection_string
  }
}

resource "random_id" "secondary_slot" {
	  byte_length = 6
}
resource "azurerm_windows_function_app_slot" "function_app_slot" {
  name                 = var.function_app_slot_name
  function_app_id      = azurerm_windows_function_app.function_app.id
  storage_account_name = azurerm_storage_account.storage_account.name
  
  site_config {
    application_stack {
      powershell_core_version = "7.2"
      
    }
    use_32_bit_worker   = false
    always_on           = true
    
  }
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  enabled                    = true
  app_settings = {
    WEBSITE_CONTENTSHARE = "${var.function_app_name}-${random_id.secondary_slot.hex}"
    WEBSITE_CONTENTAZUREFILECONNECTIONSTRING = azurerm_storage_account.storage_account.primary_connection_string
  }
}
